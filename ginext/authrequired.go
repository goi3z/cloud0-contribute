package ginext

import (
	"github.com/google/uuid"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	. "gitlab.com/goxp/cloud0/common"
)

// AuthRequiredMiddleware is required the request has to have x-user-id in header
// (it usually set by API Gateway)
func AuthRequiredMiddleware(c *gin.Context) {
	headers := struct {
		UserID   string  `header:"x-user-id" validate:"required"`
		UserMeta string `header:"x-user-meta"`
	}{}
	if c.ShouldBindHeader(&headers) != nil {
		_ = c.Error(NewError(http.StatusUnauthorized, "unauthorized"))
		c.Abort()
		return
	}

	c.Set(HeaderUserID, headers.UserID)
	c.Set(HeaderUserMeta, headers.UserMeta)

	c.Next()
}

// GetUserID returns the user ID embedded in Gin context
func GetIntUserID(c *gin.Context) (int, error) {
	tID, exists := c.Get(HeaderUserID)
	if !exists {
		return 0, c.Error(NewError(http.StatusUnauthorized, "unauthorized, userID is empty"))
	}
	tmp, _ := tID.(string)
	ID, err := strconv.Atoi(tmp)
	if err != nil {
		return 0, c.Error(NewError(http.StatusUnauthorized, "unauthorized, userID expected int type"))
	}
	return ID, nil
}

func GetUUIDUserID(c *gin.Context) (uuid.UUID, error) {
	tID, exists := c.Get(HeaderUserID)
	if !exists {
		return uuid.Nil, c.Error(NewError(http.StatusUnauthorized, "unauthorized, userID is empty"))
	}
	tmp, _ := tID.(string)
	ID, err := uuid.Parse(tmp)
	if err != nil {
		return uuid.Nil, c.Error(NewError(http.StatusUnauthorized, "unauthorized, userID expected uuid type"))
	}
	return ID, nil
}
